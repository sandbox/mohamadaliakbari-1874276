(function($){
  $.fn.dropdownHover = function(options) {
    return this.each(function() {
      var defaults = {
        delay: 500
      },
      options = $.extend(true, {}, defaults, options),
      timeout,
      $this = $(this);
      
      $this.hover(function() {
        window.clearTimeout(timeout);
        $this.addClass('open');
      }, function() {
        timeout = window.setTimeout(function() {
          $this.removeClass('open');
        }, options.delay);
      });
    });
  };
})(jQuery);